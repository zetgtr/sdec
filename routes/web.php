<?php

use Illuminate\Support\Facades\Route;
use Sdec\Http\Controllers\SdecController;



Route::middleware('web')->group(function (){
    Route::middleware('auth_pacage')->group(function () {
        Route::group(['prefix'=>"admin", 'as'=>'admin.', 'middleware' => 'is_admin'],static function(){
            Route::resource('sdec',SdecController::class);
            
        });
        Route::get('get_sdec/{cordX}/{cordY}/{postCode}', [\Sdec\Http\Controllers\SdecController::class, 'getSdec']);
    });
    try {

    } catch (Exception $exception) {
        //throw $th;
    }
});
