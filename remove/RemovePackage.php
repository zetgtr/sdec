<?php

namespace Document\Remove;

use App\Models\Admin\Menu;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class RemovePackage
{
    private $pathMigration;
    private $pathScript;
    private $pathVues;
    public function __construct()
    {
        $this->pathMigration = database_path('migrations');
        $this->pathScript = public_path('assets/js/admin/news');
        $this->pathVues = resource_path('views/vendor/news');
    }

    public function run($settings,$migration = false, $script = false, $vies = false)
    {
        chdir(base_path());
        if($migration)
        {
            $menu = Menu::query()->find(1000);
            $menu->delete();
        }

        if($script)
        {
            if (File::isDirectory($this->pathScript))
                File::deleteDirectory($this->pathScript);
        }

        if ($vies)
        {
            if (File::isDirectory($this->pathVues))
                File::deleteDirectory($this->pathVues);
        }
    }
}
