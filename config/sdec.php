<?php

use Laravel\Fortify\Features;
use Document\Enums\DocumentEnums;
use Document\Enums\CategoryEnums;

return [
    'guard' => 'web',
    'middleware' => ['web'],
    'auth_middleware' => 'auth',
    'passwords' => 'users',
    'username' => 'email',
    'email' => 'email',
    'views' => true,
    'home' => '/home',
    'prefix' => '',
    'domain' => null,
    'limiters' => [
        'login' => null,
    ],
    'redirects' => [
        'login' => null,
        'logout' => null,
        'password-confirmation' => null,
        'register' => null,
        'email-verification' => null,
        'password-reset' => null,
    ],
    'fillable'=>[
        'sdec' => [
            'client_id',
            'sicret_id',
            'postal_code'
        ]
    ],
    'request' => [
        'settings' => [
            'client_id' => 'nullable',
            'sicret_id' => 'nullable',
            'postal_code' => 'nullable'
        ]
    ],
    'features' => [
        Features::registration(),
        Features::resetPasswords(),
        Features::emailVerification(),
        Features::updateProfileInformation(),
        Features::updatePasswords(),
        Features::twoFactorAuthentication(),
    ],
];
