<?php

namespace Gallery\Seeders;
use Illuminate\Database\Seeder;
class MenuSeeder extends Seeder
{
    public function run(): void
    {
        \DB::table('menus')->insert($this->getData());
    }
    public function getData()
    {
        return [
            ['id' => 998, 'name' => 'Сдек', 'position' => 'left', 'logo' => 'fal fa-truck-container', 'controller' => 'Sdec\Http\Controllers\SdecController', 'url' => 'sdec', 'parent' => 5, "controller_type" => null, 'order' => 0]
        ];
    }
}
