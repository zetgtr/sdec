@extends('layouts.admin')
@section('title',"СДЭК")
@section('content')
    <div class="card" style="position: relative">
        <div class="card-body">
            <x-warning />
            <form action="{{ route('admin.sdec.store') }}" method="post">
                @csrf
                <x-sdec::admin.sdec.index />
                <input type="submit" value="Сохранить" class="btn btn-sm btn-success">
            </form>
        </div>
    </div>
@endsection
