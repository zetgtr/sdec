<div class="row">
    <div class="col-lg-4">
        <div class="form-groop">
            <label for="client_id">Client id</label>
            <input type="text" class="form-control" id="client_id" value="{{ $sdec ? $sdec->client_id : "" }}" name="client_id">
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-groop">
            <label for="sicret_id">Sicret id</label>
            <input type="text" class="form-control" id="sicret_id" value="{{ $sdec ? $sdec->sicret_id : "" }}" name="sicret_id">
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-groop">
            <label for="postal_code">Код города</label>
            <input type="text" class="form-control" id="postal_code" value="{{ $sdec ? $sdec->postal_code : ""}}" name="postal_code">
        </div>
    </div>
    <hr>
</div>
