<?php

namespace Sdec\Models;

use CdekSDK\CdekClient;
use Illuminate\Database\Eloquent\Model;

class Sdec extends Model
{
    protected $fillable = [];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->fillable = config('sdec.fillable.sdec');
    }
}
