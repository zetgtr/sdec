<?php

namespace Sdec\View\Admin\Sdec;

use Closure;
use Document\Models\DocumentSettings;
use Document\QueryBuilder\DocumentBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use Document\Models\Settings as Model;
use Sdec\Models\Sdec;

class Index extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct()
    {
        $this->sdec = Sdec::first();
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('sdec::components.admin.sdec.index',['sdec'=>$this->sdec]);
    }
}
