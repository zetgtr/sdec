<?php

namespace Sdec\Providers;

use App\Actions\Fortify\CreateNewUser;
use App\Actions\Fortify\ResetUserPassword;
use App\Actions\Fortify\UpdateUserPassword;
use App\Actions\Fortify\UpdateUserProfileInformation;
use App\QueryBuilder\QueryBuilder;
use App\QueryBuilder\RolesBuilder;
use App\View\Components\Admin\Photogallery\Images;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\AliasLoader;
use Sdec\Http\Middleware\Auth as PacageAuth;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Sdec\View\Admin\Sdec\Index;

class SdecServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
        }

        $this->app['router']->aliasMiddleware('auth_pacage', PacageAuth::class);

        $this->publishes([
            __DIR__.'/../../resources/views' => resource_path('views/vendor/sdec'),
        ], 'views');
        $this->publishes([
            __DIR__.'/../../resources/js' => public_path('assets/js/admin/sdec'),
        ], 'script');

        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'sdec');

        $this->loadViewsFrom(__DIR__.'/../../resources/components', 'sdec');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/api.php');
        $this->components();
//        $this->app->bind('news', function ($app) {
//            return $app->make(NewsModel::class);
//        });
    }

    private function components()
    {
        Blade::component(Index::class, 'sdec::admin.sdec.index');
    }

    private function singletons()
    {
//        $this->app->singleton(News::class, function ($app) {
//            $newsBuilder = $app->make(NewsBuilder::class);
//            return new News($newsBuilder);
//        });
//        $this->app->singleton(EditContent::class, function ($app, $parameters) {
//            $categoryBuilder = $app->make(CategoryBuilder::class);
//            $rolesBuilder = $app->make(RolesBuilder::class);
//            $newsItem = $parameters['newsItem'];
//            return new EditContent($categoryBuilder, $newsItem, $rolesBuilder);
//        });
//        $this->app->singleton(Content::class, function ($app) {
//            $categoryBuilder = $app->make(CategoryBuilder::class);
//            $rolesBuilder = $app->make(RolesBuilder::class);
//            return new Content($categoryBuilder,$rolesBuilder);
//        });
//        $this->app->singleton(Category::class, function ($app) {
//            $categoryBuilder = $app->make(CategoryBuilder::class);
//            return new Category($categoryBuilder);
//        });
//        $this->app->singleton(Settings::class, function ($app) {
//            return new Settings();
//        });
    }
    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->bind(QueryBuilder::class, DocumentBuilder::class);
//        $this->app->bind(QueryBuilder::class, CategoryBuilder::class);
        $this->mergeConfigFrom(__DIR__.'/../../config/sdec.php', 'sdec');
        $this->singletons();
    }
}
