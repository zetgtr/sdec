<?php

namespace Sdec\Request;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    public function rules(): array
    {
        return config('sdec.request.settings');
    }

    public function authorize(): bool
    {
        return true;
    }
}
