<?php

namespace Sdec\Http\Controllers;

use App\Http\Controllers\Controller;
use Sdec\Models\Sdec;
use Sdec\QueryBuilder\SdecBuilder;
use Sdec\Request\SettingsRequest;

class SdecController extends Controller
{
    public function index(){

        return view('sdec::sdec.admin.sdec.index');
    }

    public function getSdec(SdecBuilder $builder,$cordX,$cordY,$postCode){
        return $builder->getAddress($cordX,$cordY,$postCode);
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SettingsRequest $request)
    {
        Sdec::query()->updateOrCreate(['id'=>1],$request->validated());
        return back()->with('success','Настройки успешно сохранены');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, int $id)
    {
        $settings = Settings::query()->find(1);
        $product = $settings->fill($request->validated());
        if ($product->save()) {
            return \redirect()->route('admin.catalog.settings.index')->with('success', __('messages.admin.catalog.product.update.success'));
        }

        return \back()->with('error', __('messages.admin.catalog.product.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
