<?php

namespace Document\Enums;

enum CategoryEnums: string
{
    case CONTENT = 'content';
    case SEO = 'seo';
}
