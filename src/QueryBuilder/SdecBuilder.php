<?php

namespace Sdec\QueryBuilder;

use Catalog\QueryBuilder\CartBuilder;
use CdekSDK\CdekClient;
use CdekSDK\Requests\PvzListRequest;
use GuzzleHttp\Client;
use Sdec\Models\Sdec;

class SdecBuilder
{
    private $client;

    private $auth;

    public function __construct(CartBuilder $builder)
    {
        $this->sdec = Sdec::query()->first();
        $this->goods = $builder->getCount();
        if($this->sdec){
            $this->client = new CdekClient($this->sdec->client_id, $this->sdec->sicret_id);
            $url = "https://api.cdek.ru/v2/oauth/token?parameters";

            $data = array(
                'grant_type' => 'client_credentials',
                'client_id' => $this->sdec->client_id,
                'client_secret' => $this->sdec->sicret_id
            );

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($ch);
            $this->auth = json_decode($response);
        }
    }

    public function getPostCode($coordX,$coordY)
    {
        // Получаем координаты из запроса
        $latitude = $coordX;
        $longitude = $coordY;

        // Выполняем запрос к Nominatim API
        $client = new Client();
        $apiEndpoint = 'https://nominatim.openstreetmap.org/reverse';

        $response = $client->get($apiEndpoint, [
            'query' => [
                'lat' => $latitude,
                'lon' => $longitude,
                'format' => 'json',
            ],
        ]);

        $data = json_decode($response->getBody(), true);

        // Проверяем, есть ли почтовый индекс в полученных данных
        if (isset($data['address']['postcode'])) {
            $postCode = $data['address']['postcode'];
            return $postCode;
        } else {
            return 'Почтовый индекс не найден';
        }
    }

    public function getAddress($coordX,$coordY,$postCode){
        $request = new PvzListRequest();
        $request->setCityPostCode($this->getPostCode($coordX,$coordY));
        $response = $this->client->sendPvzListRequest($request);
        $pvzList = $response->getItems();
        $minDistance = INF;
        $nearestPvz = null;
        foreach ($pvzList as $pvz) {
            $distance = sqrt(pow($coordY - $pvz->coordX, 2) + pow($coordX - $pvz->coordY, 2));
            if ($distance < $minDistance) {
                $minDistance = $distance;
                $nearestPvz = $pvz;
            }
        }

        return ['sdec' => $nearestPvz, 'price' => $this->getPrice($nearestPvz->PostalCode)];
    }
    public function getPrice($cityTo){
        $weight = 200 * $this->goods;
        $url = "https://api.cdek.ru/v2/calculator/tarifflist";
        $data = array(
            "type" => 1,
            "currency" => 1,
            "lang" => "rus",
            "from_location" => array(
                "postal_code" => $this->sdec->postal_code,
            ),
            "to_location" => array(
                "postal_code" => $cityTo,
            ),
            "packages" => array(
                array(
                    "weight" => $weight,
                    "length" => 30,
                    "width" => 15,
                    "height" => 10
                )
            )
        );

        $data_string = json_encode($data);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$this->auth->access_token
        ));

        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);
        $price = 0;
        foreach ($response->tariff_codes as $item)
        {
            if($item->tariff_code === 138)
            {
                $price = $item->delivery_sum;
            }
        }

        return $price;
    }
}
